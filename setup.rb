require 'sqlite3'
require 'active_record'

#Create in-memory database for example
ActiveRecord::Base.establish_connection(
  adapter: 'sqlite3',
  database: ':memory:'
)

#define the schema
ActiveRecord::Schema.define do
  create_table :makes, force: true do |t|
    t.string  :name
  end
  
  create_table :models, force: true do |t|
    t.string      :name
    t.references  :make
  end
  
  create_table :trims, force: true do |t|
    t.string      :name
    t.references  :model
  end
end

#create classes
class ApplicationRecord < ActiveRecord::Base
  self.abstract_class = true
end

class Make < ApplicationRecord
  has_many :models

  def self.normalize_from(input_make)
    make = where("name LIKE ?", "%#{input_make.downcase}%").first
    return make.name&.titleize if make.present?

    return nil
  end

  def self.infer_from_model(input_model)
    model = Model.find_by(name: input_model.downcase)
    return model.make.&name.&titleize if model.present?

    return nil
  end
end

class Model < ApplicationRecord
  belongs_to :make
  has_many :trims

  def self.normalize_from(input)
    model = where("name LIKE ?", "%#{input.downcase}%").first
    return model.name&.titleize if model.present?
    
    parts = input.split(' ')
    return parts.length > 1 ?
      self.normalize_from(parts[0...-1].join(' ')) :
      nil
  end
end

class Trim < ApplicationRecord
  belongs_to :model

  def self.normalize_from(input_trim, input_model)
    trim = where("trims.name LIKE ?", "%#{input_trim.downcase}%").joins(:model)
      .where("models.name = ?", input_model.downcase).first
    return trim.name&.upcase if trim.present?

    return nil
  end
  
  def self.extract_from(raw_model, processed_model)
    possible_trim = raw_model.split(' ')[-1]
    trim = where("trims.name LIKE ?", "%#{possible_trim.downcase}%").joins(:model)
      .where("models.name = ?", processed_model.downcase).first
    return trim.name.upcase if trim.present?

    return nil
  end
end

#seed data
[
  {
    make: 'ford', 
    models: 
    [
      {
        name: 'focus', 
        trims:[ 'se', 'st', 'rt' ]
      }, 
      {
        name: 'taurus',
        trims: [ 'sho', 'sel' ]
      }, 
      {
        name: 'crown victoria',
        trims: [ 'lx', 'lx sport' ]
      }
    ]
  },
  {
    make: 'chevrolet', 
    models: 
    [
      {
        name: 'impala', 
        trims:[ 'st', 'ss' ]
      }, 
      {
        name: 'blazer',
        trims: [ 'k5', 's10' ]
      } 
    ]
  },
].each do |manufacturer|
  make = Make.create(name: manufacturer[:make])
  manufacturer[:models].each do |mod|
    model = make.models.create(name: mod[:name])
    mod[:trims].each do |tri|
      model.trims.create(name: tri)
    end
  end
end