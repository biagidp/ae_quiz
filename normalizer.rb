#ruby 2.7
require './setup.rb'
require 'date'

def normalize_data(input)
  car = {}
  car[:year]  = normalize_year(input[:year])
  car[:model] = normalize_model(input[:model])
  car[:make]  = normalize_make(input[:make], car[:model])
  car[:trim]  = normalize_trim(input[:trim], car[:model], input[:model])
  
  return car
end

def normalize_year(input_year)
  return nil if input_year == 'blank'
  
  year = input_year.to_i
  return year if year >= 1900 && year <= Date.today.year+2
  
  return input_year
end

def normalize_make(input_make, input_model)
  return nil if input_make == 'blank'
  
  make = Make.normalize_from(input_make)
  return make if make.present?
 
  make = Make.infer_from_model(input_model)
  return make if make.present?

  return input_make
end

def normalize_model(input_model)
  return nil if input_model == 'blank'
  
  model = Model.normalize_from(input_model)
  return model if model.present?

  return input_model
end

def normalize_trim(input_trim, processed_model, raw_model)
  return nil if input_trim == 'blank'

  trim = Trim.normalize_from(input_trim, processed_model)
  return trim if trim.present?

  trim = Trim.extract_from(raw_model, processed_model)
  return trim if trim.present?
  
  return input_trim
end

examples = [
  [{ :year => '2018', :make => 'fo', :model => 'focus', :trim => 'blank' },
   { :year => 2018, :make => 'Ford', :model => 'Focus', :trim => nil }],
  [{ :year => '200', :make => 'blah', :model => 'foo', :trim => 'bar' },
   { :year => '200', :make => 'blah', :model => 'foo', :trim => 'bar' }],
  [{ :year => '1999', :make => 'Chev', :model => 'IMPALA', :trim => 'st' },
   { :year => 1999, :make => 'Chevrolet', :model => 'Impala', :trim => 'ST' }],
  [{ :year => '2000', :make => 'ford', :model => 'focus se', :trim => '' },
   { :year => 2000, :make => 'Ford', :model => 'Focus', :trim => 'SE' }]
]

examples.each_with_index do |(input, expected_output), index|
  if (output = normalize_data(input)) == expected_output
    puts "Example #{index + 1} passed!"
  else
    puts "Example #{index + 1} failed,
          Expected: #{expected_output.inspect}
          Got:      #{output.inspect}"
  end
end