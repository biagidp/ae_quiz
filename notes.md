1.) When approaching this challenge what were some of the other ideas you came up with 
to address the problem, and why didn't you use them?

  - Directly hitting an api
    - This seems like an action we would be performing frequently, so it made sense to
      have a local copy of the data.
    - This system is designed to seed with a dump from a data source. This could be
      extended to fetch against a remote data source if information is unavailable 
      locally and cache the results.
  - Threading
    - If this is an operation that would be performed on thousands of vehicles at once 
      there are significant performance improvements to be had from utilizing threads.
    - Since it was forseeable that this method would need to be called without breaking 
      off into a new thread I left that to be implemented when the `normalize_data` 
      function is invoked.
    - Additionaly if speed is a top priority then other languages could be considered 
      for seeding as fast ruby is still relatively slow.
  - Flat data structure
    - A read heavy reference table with relatively few inserts/updates would be a good 
      candidate for denormalization, in which case a single database table could be 
      more performant. 
    - Without further information regarding the structure of the overall application 
      this is a hard call to make and so I opted to use the normalized structure. 
  - An disambiguation table for lookups of commonly used abbreviations
    - A lookup table with a polymorphic relationship to at least the makes and models 
      would allow for commonly mis-spelled or known keys to be mapped to the appropriate
      model.
    - There were no test cases where this was necessary and this would likely require 
      auditing/administration to be helpful, so I left it out.

2.) If you had, had access to other resources, what would you have chosen to use and why?

  - Vehicle VINs would allow us to lookup specific vehicle information from 
    https://vpic.nhtsa.dot.gov/api/ eliminating all guess work.
  - Greater systemic context would help shape data structures and inform decisions about 
    denormalization.
  - Failing access to VIN data, a data source with granular trim data and information 
    about valid year/model/trim combinations would be very helpful.

3.) If you chose to use a storage solution (e.g. database) what structure (e.g. tables) would you create to address this problem?

  - I used very simple makes, models, and trims tables (schema and models are viewable 
    in `setup.rb`) connected through foriegn keys. This way models/trims of the same 
    name are destinct and connected to their respective relations. This enabled the 
    fallback methods to infer makes from models so we could know that `{make: 'merc', model: 'grand marquis'}` was refering to a Mercury and not a Mercedes.